#!/usr/bin/python
# -*- coding: utf-8 -*-
import fcntl, serial
import re, json
import sys, os
import thread, time
import uuid
import traceback
import logging
from utils.tools import *

rules = None #json_load_byteified(open('mp1713c.json', 'rb'))
uart = {}
console = None
console_state = 'NONE'
rbuffer = None
wbuffer = None
console_open_prev = 0
cardinal_t = 0.5

def chksum_calc(data):
  i = 0
  chksum = ord('\x00')
  while i < len(data):
    chksum = chksum ^ ord(data[i])
    i += 1
  return chksum

def syntax_check(syntax, gets):
  if syntax == False:
    return sign_res( None, False, data=gets, msg='MALFORMED_PROTOCOL' )
  elif syntax == None:
    return sign_res( None, False, data=gets, msg='UNKNOWN_PROTOCOL' )
  else:
    return sign_res( None, True, data=gets, syntax=syntax )
    
def request_protocol_format(data):
  gets = re.findall('^([\x00-\xFF]{2})([\x00-\xFF]*)$',data )
  # 下面是處理 FC 只丟 1 Byte 的情況
  if gets == []:
    gets = [(data,'')]
  chksum = chksum_calc('$' + gets[0][0] + gets[0][1])
  syntax = format_paser(gets[0][0], gets[0][1], 'syntax')
  return syntax_check(syntax, '$' + gets[0][0] + gets[0][1] + '%02X' % chksum )

def request_paser(req):
  cmd = None
  if 'MPCmd=' in req:
    cmd = {'cmd':req.split('=')[1],'api':'custom'}
  elif '=' in req:
    requ = req.split('=')
    if rules['setup'].has_key(requ[0]):
      if rules['setup'][requ[0]].has_key(requ[1]):
        cmd = {'cmd':rules['setup'][requ[0]][requ[1]],'api':'setup'}
  elif rules['query'].has_key(req):
    cmd = {'cmd':rules['query'][req]['cmd'],'api':'query'}
  return cmd 
  
def format_paser(fc,data,mode):
  pas = {'FC': fc }
  if rules["paser"].has_key(fc):
    if rules["paser"][fc].has_key(mode):
      gets = re.findall(rules["paser"][fc][mode]["regax"],data)
      #print gets
      try:
        lenght = len(rules["paser"][fc][mode]["format"])
        for i in range(0,lenght):
          #print gets
          j = rules["paser"][fc][mode]["format"][i]
          if lenght > 1:
            pas[j] = gets[0][i]
          else:
            pas[j] = gets[i]
        return pas
      except:
        return False
    elif len(data) != 0:
      # 這是沒參數指令有參數的情況
      return False
    else:
      return pas
  return None

def response_paser(odata):
  if '\x0d' in odata:
    data = odata.split('\x0d')
    if len(data) > 2:
      #return {'_res':False, 'msg': 'RESPONSE_MUTLI_CR', 'data': odata}
      return sign_res( None, False, data=odata, msg='RESPONSE_MUTLI_CR' )
    else:
      gets = re.findall('^\!([0-9]{2})([0-9a-zA-Z\-+._]*)([0-9a-zA-Z]{2})',data[0])
      if len(gets) < 1:
        #return {'_res': False, 'msg': 'RESPONSE_FORMAT_FAIL', 'data': data[0]}
        return sign_res( None, False, data=data[0], msg='RESPONSE_FORMAT_FAIL' )
      chksum = '%02X' % chksum_calc('!' + gets[0][0] + gets[0][1])
      if chksum == gets[0][2]:
        syntax = format_paser(gets[0][0], gets[0][1],'response')
        return syntax_check(syntax, data[0])
      else:
        #return {'_res':False, 'msg': 'RESPONSE_CHKSUM_FAIL','data': data[0], 'chksum': [gets[0][2], chksum]}
        return sign_res( None, False, data=data[0], msg='RESPONSE_CHKSUM_FAIL', chksum=[gets[0][2], chksum] )
  else:
    #return {'_res':False, 'msg':'RESPONSE_NO_CR', 'data': odata}
    return sign_res( None, False, data=odata, msg='RESPONSE_NO_CR' )
    
def request_process(cmd_prefix, **dicargs):
  while rules is None:
    time.sleep(0.01)
  this_requ = {'_res': None, 'command':None, 'interface': None, 'response': None}
  this_requ['command'] = request_protocol_format(cmd_prefix)
  #this_requ = {'_res':False,'command':command,'interface': None,'response':None}
  #print this_requ
  if this_requ['command']['_res'] == False: 
    #this_requ['_res'] = False
    sign_res( this_requ, False )
    return this_requ
  
  this_data = this_requ["command"]["data"]
  if dicargs.get('cache', False) is True:
    for a in rbuffer.keys():
      try:
        if rbuffer[a]['_res'] is True and rbuffer[a]["command"]["data"] == this_data:
          logging.getLogger('root.alpp').log( logging.DEBUG, 'REQUEST 發現指令 %s 有 CACHE 存檔, 直接回應.' % this_data )
          return rbuffer[a]
      #except:
      except Exception as e:
        #print 
        logging.getLogger('root.alpp').log( logging.CRITICAL , traceback.format_exc() )
        #print traceback.print_exc()
        #pass
    #this_data = this_requ["command"]["data"]
    #print rbuffer
    #if this_data in rbuffer:
    #  if rbuffer[this_data]['_res'] is True:
    #    #print '-cache-'
    #    return rbuffer[this_data]
    
  #tStart = time.time()
  #this_requ['command']['ts'] = '%0.3f' % tStart
  tid = console_write_buffer( this_requ, dicargs.get('cache', False) )
  logging.getLogger('root.alpp').log( logging.DEBUG, 'REQUEST 產生 %s 交易機會.' % tid )
  
  while rbuffer[tid]['_res'] is None: #rbuffer[tid]['response'] is None:
    if dicargs.get('no_wait', False):
      logging.getLogger('root.alpp').log( logging.DEBUG, 'REQUEST 被設定為不等待 %s 交易結果.' % tid )
      break
      
    if time.time() - float(rbuffer[tid]['command']['ts']) >= cardinal_t * 2:
      logging.getLogger('root.alpp').log( logging.WARNING, 'REQUEST 等待超過 %0.2f 秒, 放棄 %s 交易結果.' % (cardinal_t *2, tid) )
      this_requ['response'] = {}
      sign_res( this_requ['response'], False, data=None, msg='RESPONSE_TIMEOUT' )
      sign_res( this_requ, False )
      break
    time.sleep(0.001)
  logging.getLogger('root.alpp').log( logging.DEBUG, 'REQUEST 回報 %s 交易結果.' % tid )
  return rbuffer[tid]

def check_multi_bool(req,cmd,response):
  if type(cmd) is list:
    for i in range(0,len(cmd)):
      command = cmd[i]
      if int(response[command]['response']['syntax'][rules['query'][req]['key']]) == 0:
        return 'False'
    return 'True'
  elif int(response[cmd]['response']['syntax'][rules['query'][req]['key']]) == 1:
    return 'True'
  else:
    return 'False'
    

def check_multi_res(cmd,response):
  if type(cmd) is list:
    for i in range(0,len(cmd)):
      command = cmd[i]
      if response[command]['_res'] == False:
        return False
    return True    
  return response[cmd]['_res']

def handle_request(req,cache=False):
  request = {}
  response = {}
  paser_result = request_paser(req)
  if paser_result == None:
    response = { "Return": 'Error', "msg": "UNKNOWN_PARA" }
    return json.dumps(response,sort_keys=True,indent=2)
  elif type(paser_result['cmd']) is list:
    for i in range(0,len(paser_result['cmd'])):
      request['command'] = paser_result['cmd'][i]
      response[request['command']] = request_process(request['command'],cache=cache)
  else:
    request['command'] = paser_result['cmd']
    response[request['command']] = request_process(request['command'],cache=cache)
  if paser_result['api'] == 'setup':
    response['Return'] = 'True'
    response[req] = paser_result['cmd']
  elif paser_result['api'] == 'query':
    if check_multi_res(paser_result['cmd'],response):
      if rules['query'][req]['type'] == 'bool':
        response['Return'] = check_multi_bool(req,paser_result['cmd'],response)
      elif rules['query'][req]['type'] == 'percent':
        response['Return'] = response[request['command']]['response']['syntax'][rules['query'][req]['key']] + '%'
      elif rules['query'][req]['type'] == 'int':
        response['Return'] = int(response[request['command']]['response']['syntax'][rules['query'][req]['key']])
      else:
        response['Return'] = response[request['command']]['response']['syntax'][rules['query'][req]['key']]
    else:
      response['Return'] = 'Error'

    response[req] = paser_result['cmd']
  return json.dumps(response,sort_keys=True).replace(', ', ',').replace(': ', ':')

def response_active(resp,exception_handling):
  if resp['_res'] is True:
    fc = resp['syntax']['FC']
    if rules["paser"][fc].get('effect', False) == "alarm":
      if exception_handling != None:
        exception_handling(resp)
        logging.getLogger('root.alpp').log( logging.DEBUG, '進行例外通報. (%s)' % resp['data'] )
        
    elif rules["paser"][fc].get('effect', False) == "reset_console":
    # 應變控制器啟動 conlose 關閉
      console_close('CONSOLE_RESET')
      logging.getLogger('root.alpp').log( logging.INFO, '準備進行重置.' )
      time.sleep(5)

      
def console_set_state(message):
  global console_state
  if console_get_state() != message:
    logging.getLogger('root.alpp').log( logging.INFO, '狀態由 %s 轉變為 %s .' % ( console_state, message ) )
    console_state = message

def console_get_state():
  return console_state
  
def console_close(message=None):
  global console
  console.close()
  console = None
  if message is not None:
    console_set_state(message)
  else:
    console_set_state('CONSOLE_CLOSE')

def console_open():
  global console, console_open_prev
  # 最多一秒開一次.
  if time.time() > console_open_prev + 1:
    console_open_prev = time.time()
    try:
      console = serial.Serial(port=uart['port'],baudrate=uart['baudrate'], bytesize=uart['bytesize'], parity=uart['parity'], stopbits=uart['stopbits'], timeout=uart['timeout'], xonxoff=0, rtscts=0)
      
    except Exception as e:
      logging.getLogger('root.alpp').log( logging.ERROR, '開啟發生異常.' )
      return False
    #console.set_low_latency_mode( True )
    
    #檢查 BUSY
    if console is not None:
      if console.isOpen():
        import fcntl
        try:
          fcntl.flock(console.fileno(), fcntl.LOCK_EX | fcntl.LOCK_NB)
        except IOError:
          console_close('CONSOLE_BUSY')
          logging.getLogger('root.alpp').log( logging.ERROR, '設備 %s 忙碌中.' % uart['port'] )
          return False
          
    console_set_state('CONSOLE_ACTIVE')
    return True
    
  else:
    time.sleep(0.02)
    return False
    
def console_write_buffer( this_requ, insert=False ):
  global wbuffer
  global rbuffer
  this_uuid = str(uuid.uuid4())
  rbuffer[this_uuid] = this_requ
  if insert is True:
    wbuffer.insert( 0, this_uuid )
    
  else:
    wbuffer.append( this_uuid )
    
  return this_uuid

def console_read():
  if console is None:
    if console_open() is False:
      return ''
  try:
    rb = console.read(1)
  except Exception as e:
    logging.getLogger('root.alpp').log( logging.ERROR, '異常錯誤: %s' % e )
    console_close()
    return ''
  return rb

def console_parameter(**dicargs):
  global uart
#  print dicargs
  default_conf = {'port':'/dev/ttyUSB0','baudrate':'115200','bytesize':8,'parity':'N','stopbits':1,'timeout':0.02}
  for parameter in default_conf:
    if dicargs.get(parameter, False) == False:
      uart[parameter] = default_conf[parameter]
    else:
      uart[parameter] = dicargs[parameter]
    logging.getLogger('root.alpp').log( logging.DEBUG, '參數: %s, 值: %s' % (parameter, uart[parameter]) )
          

def uart_service(**dicargs):
  global rbuffer, wbuffer, rules
  thread_name_init()
  wbuffer = list()
  rbuffer = dict()
  rules = dicargs.get('rules', json_load_byteified(open('rules/mp1713c.json', 'rb')))
  logging.getLogger('root.alpp').log( logging.INFO, '啟動.' )
  '''
  if dicargs.get( 'external_dict', None ) != None:
    rbuffer = dicargs['external_dict']
    for a in rbuffer.keys():
      del rbuffer[a]
  else:
    rbuffer = {}
  '''
  
  console_parameter(**dicargs)
  response_data = ''
  first_tid = None
  
  while True:
    character = console_read()
    response_data += character
    if character == '\r':
      resp = response_paser(response_data)
      logging.getLogger('root.uart').debug('[RECV] ' + response_data[:-1])
      
      this_fc = None
      try :
        this_fc = resp['syntax']['FC']
      except KeyError:
        # Key is not present
        pass
      
      tid_fc = None
      try :
        tid_fc = rbuffer[first_tid]["command"]['syntax']['FC']
      except KeyError:
        # Key is not present
        pass
      
      if this_fc == tid_fc != None:
        rbuffer[first_tid]['response'] = resp
        sign_res( rbuffer[first_tid], rbuffer[first_tid]['response']['_res'] )
        logging.getLogger('root.alpp').log( logging.DEBUG, '與對象的 %s 交易完成. (%s)' % ( first_tid, response_data[:-1] ))
        first_tid = None
        
      else:
        #例外狀況A
        logging.getLogger('root.alpp').log( logging.DEBUG, '收到對象發送主動訊息. (%s)' % response_data[:-1] )
        pass
         
      response_active(resp, dicargs.get('exception_handling', None))
      response_data = ''
    
    if first_tid == None:
      ts = time.time()
      for a in rbuffer.keys():
        tid_ts = rbuffer[a].get('ts', 0)
        # History 保留 1 + cardinal_t 秒.
        if tid_ts != 0 and ts >= float( tid_ts ) + 1 + ( cardinal_t * 2 ):
          del rbuffer[a]
          
      if len(wbuffer) > 0:
        first_tid = wbuffer[0]
        rbuffer[first_tid]['interface'] = {}
        if console is not None:
          console_type = {'port': console.port,'config': str(console.baudrate) + '-' + str(console.bytesize) + '-'+ console.parity + '-' + str(console.stopbits), 'timeout': console.timeout}
          sign_res(rbuffer[first_tid]['interface'], True, console=console_type )
          console.write( rbuffer[first_tid]["command"]["data"] + '\x0d')
          logging.getLogger('root.uart').debug('[SEND] ' + rbuffer[first_tid]["command"]["data"])
          logging.getLogger('root.alpp').log( logging.DEBUG, '狀態正常, 進行 %s 交易. (%s)' % ( wbuffer[0], rbuffer[first_tid]["command"]["data"] ))
          wbuffer.remove( first_tid )
        else:
          sign_res( rbuffer[first_tid]['interface'], False, msg=console_state )
          sign_res( rbuffer[first_tid], False )
          logging.getLogger('root.alpp').log( logging.WARNING, '狀態異常, 放棄 %s 交易. (%s)' % ( wbuffer[0], rbuffer[first_tid]["command"]["data"] ) )
          wbuffer.remove( first_tid )
          first_tid = None

    elif first_tid != None:
      # 超過 cardinal_t 秒放棄這筆交易. 
      if time.time() >= float(rbuffer[first_tid]['interface']['ts']) + cardinal_t:
        sign_res( rbuffer[first_tid]['response'], False, msg='RESPONSE_TIMEOUT' ) 
        sign_res( rbuffer[first_tid], False ) 
        logging.getLogger('root.alpp').log( logging.WARNING, '對象超過 %0.2f 秒無回應, 放棄 %s 交易. (%s)' % (cardinal_t, first_tid, rbuffer[first_tid]['command']['data'] ))
        first_tid = None
        
def sign_res( d, res, **dicargs):
  #print d
  if d == None:
    d = {}
  d['_res'] = res
  d['ts'] = '%0.3f' % time.time()
  for a in dicargs:
    d[a] = dicargs[a]
  return d
  


if __name__ == '__main__':
  from threading import Thread

  thread1 = Thread(target=uart_service,kwargs={'port': '/dev/ttyUSB0'})
  thread1.daemon = True
  thread1.start()
  #time.sleep(10)
  print handle_request(sys.argv[1])