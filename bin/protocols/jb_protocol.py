#!/usr/bin/python
# -*- coding: utf-8 -*-
import logging, os
import time 
import al_protocol
import json
import traceback
import uuid
import subprocess
import ssl
import paho.mqtt.client as mqtt
from threading import Thread
from utils.tools import *

topic_root = None
client = None
handle_request = None
service = {}
reqActive = []
resTimeout = 0
rules = None #json_load_byteified(open('jbp.json', 'rb'))
args = None
check_sign = False
sign_binary = None
tls_connect = False

wdt_ts = 240 #1-255

def inquiry( rule, data=None ):
  if len(rule) > 0:
    rule_copy = dict( rule )
    for this in rule:
      if type(rule[this]) is dict:
        action = rule[this].get("action", False)
        if action == "query":
          resp = al_protocol.handle_request( rule[this]["target"], True )
          rule_copy[this] = json_loads_byteified(resp)["Return"]
        elif action == "transparent":
          rule_copy[this] = data[this]
        elif action == "get":
          rule_copy[this] = args.get( rule[this]["target"], 'None')
  else:
    rule_copy = dict()
  return rule_copy

def configure( rule, parameters ):
  for this in parameters.keys():
    if rule.get(this, False) != False:
      action = rule[this].get("action", False)
      if action == "setup":
        b = al_protocol.handle_request( rule[this]["target"] + '=' + parameters[this] )
        parameters[this] = json_loads_byteified(b)["Return"]
      
      elif action == "transparent":
        parameters[this] = al_protocol.request_process(parameters[this]) 

      elif action == "service_enable":
        api = parameters[this]
        if api == "inputPinStatus":
          global resTimeout
          service[api] = parameters
          if service[api].get("resTimeout", None) != None:
            resTimeout = float(service[api]["resTimeout"])
            del service[api]["resTimeout"]
          else:
            resTimeout = float( 1 )
          
          # upper/lower 強制初始狀態為 0/1 , 啟動後可直接觸發第一次狀態.
          for di in service[api]:
            if type(service[api][di]) is dict:
              trigger = service[api][di].get("trigger", None)
              if trigger == "upper":
                service[api][di]['N_DI'] = 0
                service[api][di]['O_DI'] = 0
              elif trigger == "lower":
                service[api][di]['N_DI'] = 1
                service[api][di]['O_DI'] = 1
          check_hw_di()
            
          del service[api]["api"]
          
        elif api == "watchdog":
          set_watchdog_state(True, int(parameters.get("timer", wdt_ts)))
          
      elif action == "service_disable":
        api = parameters[this]
        if service.get(api, False) !=  False:
          del service[api]
        if api == "watchdog":
          set_watchdog_state(False)
        
#    else:
#      parameters[this] = parameters[this] #"Error" #"UNKNOWN_REQU"

def original_handle_request( json_object ):
  api = json_object.get( "api", 'null' )
  if api != 'null' and rules["mqtt"]["passive"].get( api, False ) != False:
    response = dict(rules["syntax"]["response"])
    response["api"] = api
    response["reqSn"] = json_object["reqSn"]
    response["unixTime"] = "%d" % time.time()
    this_sign = generate_sign(json_object)
    if json_object["sign"] == this_sign or not check_sign:
      configure(rules["mqtt"]["passive"][api]["request"], json_object["data"])
      response["retVal"] = inquiry(rules["mqtt"]["passive"][api]["response"],json_object["data"])
      if rules["mqtt"]["passive"][api].get( 'effect', None ) == 'WDR':
        if service.get('watchdog', False) == False and args.get('auto_wdt', False) is True:
          set_watchdog_state( True )
        wdr()
    else:
      logging.getLogger('root.jbpp').log( logging.WARNING, '驗證碼檢查異常, 正常: %s , 異常: %s .' % (this_sign, json_object["sign"]) )
      response["retCode"] = "0"
      response["retMsg"] = "CHKSUM_ERROR"

  else:
    logging.getLogger('root.jbpp').log( logging.WARNING, 'UNKNOWN_REQU: %s .' % api )
    response = dict()
    response["api"] = api
    response["reqSn"] = json_object.get('reqSn', 'null')
    response["unixTime"] = "%d" % time.time()
    response["retVal"] = {}
    response["retCode"] = "0"
    response["retMsg"] = "UNKNOWN_REQU"
  response["sign"] = generate_sign(response)
  
  return json_dump(response)

def get_watchdog_state():
  try:
    resp = al_protocol.request_process("05")
    if resp['response'] is not None:
      if resp['response']['syntax']['S'] == '1':
        return True
      elif resp['response']['syntax']['S'] == '0':
        return False
  except:
    logging.getLogger('root.jbpp').log( '指令 05 回應異常.' )
    return False
def wdr():
  al_protocol.request_process("04", no_wait=True, cache=True )
  if service.get('watchdog', False) != False:
    service["watchdog"]['timeout'] = time.time() + service['watchdog']['timer']
    logging.getLogger('root.jbpp').log( logging.DEBUG, '餵笨狗.' )

def set_watchdog_state(state,ts=wdt_ts):
  #global wdt 
  if state is True:
    logging.getLogger('root.jbpp').log( logging.INFO, '開門狗功能啟動.' )
    al_protocol.request_process("03%03d" % ts, no_wait=True, cache=True )
    if service.get('watchdog', None) != None:
      service['watchdog']['timer'] = ts
      service['watchdog']['timeout'] = time.time() + ts
    else:
      service['watchdog'] = {}
      service['watchdog']['timer'] = ts
      service['watchdog']['timeout'] = time.time() + ts
    return True
  elif state is False:
    logging.getLogger('root.jbpp').log( logging.INFO, '開門狗功能關閉.' )
    al_protocol.request_process("03000", no_wait=True, cache=True )
    #auto_wdt = False
    if args.get('auto_wdt', False) != False:
      args['auto_wdt'] = False
    if service.get('watchdog', None) != None:
      service.remove('watchdog')
    return True
  return False
   
def generate_sign(json_object):
  if sign_binary is not None:
    cmd = sign_binary + ' -t ' + json_object['unixTime'] + ' -d \'' + json_dump(json_object) + '\''
  
  else:
    cmd = 'echo ' + json_object['unixTime'] + json_dump(json_object) + ' | sha256sum'
    
  sign =  subprocess.check_output(cmd,shell=True).split(' ')[0].upper()
  logging.getLogger('root.jbpp').log( logging.DEBUG, '%s => %s' % (cmd, sign) )
  return sign
  
def generate_password(string):
  if sign_binary is not None:
    cmd = sign_binary + ' -pwd ' + string 
    
  else:
    cmd = 'echo ' + string + ' | sha256sum'
    
  pwd =  subprocess.check_output(cmd,shell=True).split(' ')[0].upper()
  logging.getLogger('root.jbpp').log( logging.DEBUG, '%s => %s' % (cmd, pwd) )
  return pwd
  
def exception_handling(data):
  try:
    if data['_res'] is True:
      FC = data['syntax']['FC']
      I = data['syntax']['I']
      device = rules["exception_handling"][FC]['device'][I]
      syntax = rules["exception_handling"][FC]['target_syntax']
      # IPS 存在於 service 結構, 且偵測被定義為 1 , 且  N_DI (新的準位) 有變化.
      if "inputPinStatus" in service and service["inputPinStatus"].get(device, False) and service["inputPinStatus"][device].get("detect", "0") == "1" and service["inputPinStatus"][device].get('N_DI', -1) != int(data['syntax'][syntax]):
        service["inputPinStatus"][device]['N_DI'] = int(data['syntax'][syntax])
        service["inputPinStatus"][device]['N_TS'] = float(data['ts'])
        # 第一次收到訊息舊的準位該怎麼判定, 被動與主動的差別.
        if service["inputPinStatus"][device].get('O_DI', None) == None:
          # 下面這個 if 不應該會再發生.
          if FC == "22":
            service["inputPinStatus"][device]['O_DI'] = abs( service["inputPinStatus"][device]['N_DI'] - 1 )
          else:
            service["inputPinStatus"][device]['O_DI'] = service["inputPinStatus"][device]['N_DI']
  
  except Exception as e:
    logging.getLogger('root.jbpp').log( logging.CRITICAL, '%s' % traceback.format_exc() )
    logging.getLogger('root.jbpp').log( logging.CRITICAL, '%s' % str(data) )
    pass
    
def reqactive_service(ips_active_inquiry=False):
  thread_name_init()
  logging.getLogger('root.jbpp').log( logging.INFO, '啟動.' )
  #ips_active_inquiry = False #True #False
  if ips_active_inquiry is True:
    logging.getLogger('root.jbpp').log( logging.INFO, '為主動詢問模式.' )
    al_protocol.request_process("2110", cache=True)
    al_protocol.request_process("2120", cache=True)
    al_protocol.request_process("2130", cache=True)
    al_protocol.request_process("2140", cache=True)
  else:
    logging.getLogger('root.jbpp').log( logging.INFO, '為被動通知模式.' )
    al_protocol.request_process("2111", cache=True)
    al_protocol.request_process("2121", cache=True)
    al_protocol.request_process("2131", cache=True)
    al_protocol.request_process("2141", cache=True)
    
  ts = long(int(time.time()))
  while True:
    if len(service) >= 1:
      if service.get( "inputPinStatus", False ) !=  False:
        data = service["inputPinStatus"]
        for a in data:
          try:
            # 結構要為 dict, 且新舊 GPIO 值不一樣且同時不得為空, 且變化超過 steadyState 時間.
            if type(data[a]) is dict and data[a].get('N_DI', None) != data[a].get('O_DI', None) != None and data[a].get('N_TS', None) != None and ( time.time() - data[a]['N_TS'] ) >= float(int(data[a]['steadyState']))/1000:
              trigger = data[a]["trigger"] 
              if trigger == "both" or ( ( trigger == "falling" or trigger == "lower" ) and data[a]['N_DI'] == 0 ) or ( ( trigger == "rising" or trigger == "upper" ) and data[a]['N_DI'] == 1 ):
                request = dict(rules["syntax"]["request"])
                request["api"] = "reqActive"
                request["reqSn"] = str(uuid.uuid4())
                request["unixTime"] = "%d" % time.time()
                request["data"] = {"api": "inputPinStatus", a:  str(data[a]['N_DI'])}
                request["sign"] = generate_sign(request)
                logging.getLogger('root.jbpp').log( logging.INFO, '目標 %s 滿足觸發條件, 訊號為 %s , 交易序號 %s .' % ( a, data[a]['N_DI'], request["reqSn"] ) )
                Thread(target=request_process, args=[json_dump(request),"request"], name='mqtt_request').start()
              data[a]['O_DI'] = data[a]['N_DI']
          except Exception as e:
            logging.getLogger('root.jbpp').log( logging.CRITICAL , traceback.format_exc() )

    #500ms timebase
    if time.time() > ts:
      ts = ts + 0.5
      # 要添加如果 ips_active_inquiry 是 False, 但是 O_DI 為空值時要去填值.
      if ips_active_inquiry is True:
        check_hw_di()
      if service.get( 'watchdog', None ) != None and 2 > service['watchdog']['timeout'] - time.time() > 0:
        logging.getLogger('root.jbpp').log( logging.WARNING , '汪汪汪汪汪!!' )
      with open( '/tmp/plc.service', 'w' ) as file:
        json.dump( service, file, sort_keys = True ,indent = 2 )
      with open( '/tmp/plc.service.resTimeout', 'w' ) as file:
        file.write(str(resTimeout))

    time.sleep(0.01)

def check_hw_di():
  al_protocol.request_process("201", no_wait=True)
  al_protocol.request_process("202", no_wait=True)
  al_protocol.request_process("203", no_wait=True)
  al_protocol.request_process("204", no_wait=True)

    
def request_process(object,action):
  thread_name_init()
  logging.getLogger('root.jbpp').log( logging.DEBUG, '啟動.' )
  try:
    if type( object ) is not dict:
      object = json_loads_byteified( object )
    if type( object ) is not dict:
      raise
  except:
    logging.getLogger('root.jbpp').log( logging.WARNING, '無法解析: %s' % ( object ) )
    
  if type( object ) is dict and action == "request":
    count = 0
    tPart = 0
    reqSn = object["reqSn"]
    logging.getLogger('root.jbpp').log( logging.DEBUG, '收到 %s 發送申請.' % ( reqSn ) )
    while True:
      if time.time() >= tPart:
        tPart = time.time() + resTimeout
        client.publish( topic_root + 'pub', json_dump(object)) #, qos=1)
        
        if count == 0:
          logging.getLogger('root.jbpp').log( logging.DEBUG, '發送 %s 資料.' % ( reqSn ) )
        else:
          logging.getLogger('root.jbpp').log( logging.DEBUG, '發送 %s 資料, 重送第 %d 次.' % ( reqSn, count ) )
        count = count + 1
        
      #if 收到回應 OK
      
      if reqSn in reqActive:
        reqActive.remove(reqSn)
        logging.getLogger('root.jbpp').log( logging.DEBUG, '確認收到 %s 回報.' % ( reqSn ) )
        break
        
      #3 次嘗試失敗, Log 紀錄
      if count > 3:
        logging.getLogger('root.jbpp').log( logging.WARNING, '放棄等待 %s 回報.' % ( reqSn ) )
        break
          
      time.sleep(0.01)
      
  elif type( object ) is dict and action == "response":
    if object.get('api', None) == 'reqActive':
      reqSn = object.get("reqSn", 'null')
      this_sign = generate_sign(object)
      if object["sign"] == this_sign or not check_sign:
        logging.getLogger('root.jbpp').log( logging.DEBUG, '紀錄 %s 交易請求.' % ( reqSn ) )
        if type(reqSn) is str:
          reqActive.append( reqSn )
      else:
        logging.getLogger('root.jbpp').log( logging.WARNING, '驗證碼檢查異常, 正常: %s , 異常: %s .' % (this_sign, object["sign"]) )
        
    else:
      reqSn = object.get("reqSn", 'null')
      client.publish( topic_root + 'pub', handle_request(object)) #, qos=1)
      logging.getLogger('root.jbpp').log( logging.INFO, '回傳 reqSn-%s 交易結果.' % ( reqSn ) )
  logging.getLogger('root.jbpp').log( logging.DEBUG, '結束.' )

def on_connect(mq, userdata, rc, _):
  mq.subscribe( topic_root +'#')

def on_message(mq, userdata, msg):
  logging.getLogger('root.mqtt').log( logging.DEBUG, '[%s] %s' % (msg.topic, msg.payload) )
  if msg.topic.rpartition('/')[-1] == 'sub':
    try:
      object = json_loads_byteified( msg.payload )
      logging.getLogger('root.jbpp').log( logging.INFO, '收到 reqSn-%s 交易請求.' % object.get("reqSn", 'null') )
    except:
      object = msg.payload
      logging.getLogger('root.jbpp').log( logging.WARNING, '收到非 JSON 格式交易請求.' )
      
    Thread(target=request_process, args=[object,"response"],name='mqtt-response').start()
    
def mqtt_service(**dicargs): 
  global handle_request, client, topic_root, rules, args, check_sign, sign_binary, service, resTimeout, tls_connect
  args = dicargs
  thread_name_init()
  logging.getLogger('root.jbpp').log( logging.INFO, '啟動.' )
  
  if os.path.isfile('/tmp/plc.service'):
    with open( '/tmp/plc.service', 'r' ) as file:
      try:
        service = json_load_byteified( file )
        logging.getLogger('root.jbpp').log( logging.WARNING, '暫存設定 plc.service 存在, 已載入.' )
      except:
        logging.getLogger('root.jbpp').log( logging.ERROR, '暫存設定 plc.service 存在, 讀取失敗.' )

  if os.path.isfile('/tmp/plc.service.resTimeout'):
    with open( '/tmp/plc.service.resTimeout', 'r' ) as file:
      try:
        resTimeout = float(file.read())
        logging.getLogger('root.jbpp').log( logging.WARNING, '暫存設定 plc.service.resTimeout 存在, 已載入.' )
      except:
        logging.getLogger('root.jbpp').log( logging.ERROR, '暫存設定 plc.service.resTimeout 存在, 讀取失敗.' )
        
  if get_watchdog_state() is True:
    logging.getLogger('root.jbpp').log( logging.WARNING, '門前已有狗!!' )
    set_watchdog_state( True )
  
  sign_binary = dicargs.get('sign_binary', sign_binary)
  if os.path.isfile(sign_binary) is False:
    logging.getLogger('root.jbpp').log( logging.WARNING, '憑證演算執行檔不存在, 憑證演算驗證無法啟用.' )
    sign_binary = None
  else:
    check_sign = dicargs.get('check_sign', check_sign)

  tls_connect = dicargs.get('mqtt_tls', tls_connect)
  
  rules = dicargs.get('handle_request', json_load_byteified(open('rules/jbpp.json', 'rb')))
  handle_request = dicargs.get('handle_request', original_handle_request)

  user = ("%03d" % (int ( get_host_ip ( ).split ( '.' )[ 3 ] )) + "%0.6f" % (time.time ( ))).replace ( '.' , '' )
  password = generate_password ( user )
  client = mqtt.Client ( client_id=user ) ;
  logging.getLogger ( 'root.jbpp' ).log ( logging.INFO , '使用者名稱為 %s, 密碼為 %s .' % (user , password) )
  client.username_pw_set ( user , password )

  topic_root = "%03d" % (int ( get_host_ip ( ).split ( '.' )[ 3 ] )) + '/plc/'
  logging.getLogger ( 'root.jbpp' ).log ( logging.INFO , '訂閱頻道為 %s .' % topic_root )
  client.on_connect = on_connect
  client.on_message = on_message
  
  # 這邊需要一個無窮檢查式, 自動重新連線.
  try:
    host = dicargs.get('host', 'iot.eclipse.org')
    if tls_connect is True:
      port = dicargs.get('port')
      if port is None:
        port = 8443
      client.tls_set(cert_reqs=ssl.CERT_NONE)

    else:
      port = dicargs.get('port')
      if port is None:
        port = 1883

    client.connect(host,port)
    logging.getLogger('root.jbpp').log( logging.INFO, '連線到 %s:%d 成功.' % (host,port) )
  except:
    logging.getLogger('root.jbpp').log( logging.ERROR, '連線 %s:%d 異常.\n%s' % (host,port,traceback.format_exc()) )
    
  client.loop_forever()
  while True:
    time.sleep(1)
  
  client.disconnect()



if __name__ == '__main__':
  time.sleep(1)

