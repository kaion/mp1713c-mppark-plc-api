#!/usr/bin/python
# -*- coding: utf-8 -*-
import json
import socket
import ctypes
import threading
import os
import fcntl
import sys
import re

file_lock = None

def config_get(config,section,option,is_null=None):
  if not config.has_section(section): 
    value = is_null
  elif not config.has_option(section, option):
    value = is_null
  else:
    value = config.get(section, option)

  if value is not None:
    try:
      value = int(value)
    except ValueError:
      if value.lower() == 'true':
        value = True
      elif value.lower() == 'false':
        value = False
  #print '%s,%s,%s,%s' % (section,option,value,type(value))
  return value
  
def set_abspath_chdir(file):
  # 當前目錄設定 bin 底下.
  abspath = os.path.abspath(file)
  end_index = abspath.rfind('/')
  #print abspath[:end_index]
  os.chdir(abspath[:end_index])
  
def check_repeated_execution(file):
  global file_lock
  file_lock = open(os.path.realpath(file), "r")
  try:
    fcntl.flock( file_lock, fcntl.LOCK_EX | fcntl.LOCK_NB )
    #print '此程式鎖定.'
  except IOError:
    print '此程式已在運行中.'
    sys.exit(1)
    
  return file_lock
  
def thread_name_init(name=None):
  this_thread = threading.current_thread()
  if name is None:
    this_thread.setName( '%d-%s' % (get_threaing_pid(), this_thread.getName()) )
  else:
    this_thread.setName( '%d-%s' % (get_threaing_pid(), name) )
    
def get_today_midnight():
  from datetime import date, datetime, time
  return datetime.combine(date.today(), time.max)
  
def test1():
    #round(number,1)
    interval = 0.01
    if next_ts == None:
      next_ts = round(time.time() + 0.005, 2)
    #else:
    next_ts = next_ts + interval
    delay_ts = next_ts - time.time()
    while delay_ts <= 0:
      next_ts = next_ts + interval
      delay_ts = next_ts - time.time()
      print 'W: 超時'
    #print delay_ts
    time.sleep(delay_ts)
    
  
def get_threaing_pid():
  #grep -r '__NR_gettid' /usr/include/ 
  #return ctypes.CDLL('libc.so.6').syscall(224) 
  try:
    import platform, ctypes
    if not platform.system().startswith('Linux'):
      raise ValueError
    syscalls = {
      'i386': 224,
      'x86_64': 186,
      'armv7l': 224
    }
    path = ctypes.util.find_library("c")
    mach = platform.machine()
    pid = ctypes.CDLL(path).syscall(syscalls[mach])
  except:
    pid = -1
  return pid

def get_host_ip():
  try:
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.connect(('8.8.8.8', 80))
    ip = s.getsockname()[0]
  finally:
    s.close()
  return ip

def json_load_byteified(file_handle):
  return _byteify(
    json.load(file_handle, object_hook=_byteify),
    ignore_dicts=True
  )

def json_loads_byteified(json_text):
  return _byteify(
    json.loads(json_text, object_hook=_byteify),
    ignore_dicts=True
  )
  
def json_dump(json_object):
  return json.dumps(json_object, sort_keys=True).replace(', ', ',').replace(': ', ':')

def _byteify(data, ignore_dicts = False):
  # if this is a unicode string, return its string representation
  if isinstance(data, unicode):
    return data.encode('utf-8')
  # if this is a list of values, return list of byteified values
  if isinstance(data, list):
    return [ _byteify(item, ignore_dicts=True) for item in data ]
  # if this is a dictionary, return dictionary of byteified keys and values
  # but only if we haven't already byteified it
  if isinstance(data, dict) and not ignore_dicts:
    return {
      _byteify(key, ignore_dicts=True): _byteify(value, ignore_dicts=True)
      for key, value in data.iteritems()
    }
  # if it's anything else, return it in its original form
  return data

if __name__ == '__main__':
  print get_host_ip()