#!/usr/bin/python
# -*- coding: utf-8 -*-

# this_version = '20180907'

# 20181012
# 將 mqtt client and user 使用相同
# 20190307 1.0.1
# 修正 08 指令回應過長導致回應 timeout 問題

this_version = '1.0.1'

from protocols import al_protocol, jb_protocol
from utils.tools import json_loads_byteified
  
def handle_request( object ):
  try:
    object = json_loads_byteified( object )
  except Exception as e:
    pass
    
  if type( object ) is str :
      return al_protocol.handle_request( object )
  
  elif type( object ) is dict :
    return jb_protocol.handle_request( object )
    
  else:
    return None


if __name__ == '__main__':
  import logging, time, os, sys, resource, dotenv
  from utils import logmanager, httpd
  from protocols import al_protocol, jb_protocol
  from threading import Thread
  from utils.tools import *
  from ConfigParser import RawConfigParser
  
  check_repeated_execution(sys.argv[0])
  set_abspath_chdir(sys.argv[0])
  print 'Starting server, use <Ctrl-C> to stop'
  
  config_ini = RawConfigParser ()
  if os.path.isfile('config.ini'): 
    config_ini.read('config.ini')
  else:
    config_ini.read('default_config.ini')
    
  plc ={
    'threads' : {
      'uart-service': {
        'target': al_protocol.uart_service,
        'kwargs': {
#          'rules': json_load_byteified(open('rules/mp1713c.json', 'rb')
          'port': config_get( config_ini,'PLC','UART','/dev/ttyUSB0'),
          'exception_handling': jb_protocol.exception_handling
        }
      },
      'mqtt-service': {
        'target': jb_protocol.mqtt_service,
        'kwargs': {
#          'rules': 'json_load_byteified(open('rules/jbpp.json', 'rb')
#          'external_handle_request': jb_protocol.handle_request
          'auto_wdt': config_get(config_ini,'PLC','AUTO_WDT',False),
          'host': config_get(config_ini,'MQTT','BROKER_IP', '127.0.0.1'),
          'port': config_get(config_ini,'MQTT','BROKER_PORT', None),
          'check_sign': config_get(config_ini,'MQTT','CHECK_SIGN',True),
          'sign_binary': config_get(config_ini,'MQTT','SIGN_BINARY','/usr/local/mppark/bin/program'),
          'swVersion': this_version,
          'mqtt_tls': config_get(config_ini,'MQTT','MQTT_TLS',False)
        }
      },
      'reqactive-service': {
        'target': jb_protocol.reqactive_service,
        'kwargs': {
          'ips_active_inquiry' : config_get(config_ini,'PLC','IPS_ACTIVE_INQUIRY',False)
        }
      },
      'logging-service': {
        'target': logmanager.service,
        'kwargs': {
          'task': {
            'uart': {
              'level' : config_get(config_ini,'LOGGING','UART_SERVICE_LV',10),
              'format' : '%(asctime)s.%(msecs)03d %(message)s',
            },
            'mqtt': {
              'level' : config_get(config_ini,'LOGGING','MQTT_SERVICE_LV',10),
              'format' : '%(asctime)s.%(msecs)03d %(message)s',
            },
            'alpp': {
              'level' : config_get(config_ini,'LOGGING','AL_PROTOCOL_LV',20),
              'format' : '%(asctime)s.%(msecs)03d [%(levelname)-.4s] %(threadName)s %(message)s',
            },
            'jbpp': {
              'level' : config_get(config_ini,'LOGGING','JB_PROTOCOL_LV',20),
              'format' : '%(asctime)s.%(msecs)03d [%(levelname)-.4s] %(threadName)s %(message)s',
            },
            'logg': {
              'level' : config_get(config_ini,'LOGGING','LOGGING_SERVICE_LV',20),
              'format' : '%(asctime)s.%(msecs)03d [%(levelname)-.4s] %(threadName)s %(message)s',
            }
          },
          'max_file_bytes' : config_get(config_ini,'LOGGING','MAX_FILE_BYTES',10) * 1024 * 1024,
          'max_directory_bytes' : config_get(config_ini,'LOGGING','MAX_DIRECTORY_BYTES',20) * 1024 * 1024,
        }
      },
      'httpd': {
        'target': httpd.test,
        'kwargs': {
          'response_function' : handle_request
        }
      }
    },
    'enable_sequence' : [
      'logging-service',
      'uart-service',
      'mqtt-service',
      'reqactive-service',
    ]
  }
  if config_get(config_ini,'HTTPD','ENABLE',False) is True:
    plc['enable_sequence'].append('httpd')
    
  thread_name_init('main')
  ts = 0
  while True:
    for thread_name in plc['enable_sequence']:
      if plc['threads'].get(thread_name, None) != None:
        if plc['threads'][thread_name].get('thread', None) == None or plc['threads'][thread_name]['thread'].isAlive() is False:
          plc['threads'][thread_name]['thread'] = Thread(target=plc['threads'][thread_name]['target'], kwargs=plc['threads'][thread_name]['kwargs'], name=thread_name)
          plc['threads'][thread_name]['thread'].daemon = True
          plc['threads'][thread_name]['thread'].start()
          time.sleep(0.1)
    if time.time() > ts:
      if ts == 0:
        logging.getLogger('root.logg').log( logging.INFO, '目前版本: %s .' % this_version )
        ts = time.time()
      ts = ts + ( 60 * 30 )
      usage_memory = resource.getrusage(resource.RUSAGE_SELF).ru_maxrss
      if usage_memory >= 20 * 1024:
        logging.getLogger('root.logg').log( logging.WARNING, '程式記憶體使用量: %s MB' % format( float(usage_memory) / 1024, '0,.2f') )
      else:
        logging.getLogger('root.logg').log( logging.INFO, '程式記憶體使用量: %s MB' % format( float(usage_memory) / 1024, '0,.2f') )

    time.sleep(1)

